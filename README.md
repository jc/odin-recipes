This is an Odin Project project webpage, focusing on linking between various websites. The example here is a recipe website referring to specific recipe webpages.

And since I am a vegan and we are inherently very open about that - some say "in the face" - all of these recipes are vegan adaptations to the original carnivore versions. 

Enjoy.
